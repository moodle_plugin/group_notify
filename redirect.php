<?php
// This file is part of MailTest for Moodle - http://moodle.org/
//
// MailTest is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// MailTest is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with MailTest.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays the form and processes the form submission.
 *
 * @package    local_mailtest
 * @copyright  2015-2017 TNG Consulting Inc. - www.tngconsulting.ca
 * @author     Michael Milette
 * @license    http://www.gnu.org/copyleft/gpl.html  GNU GPL v3 or later
 */

// Include config.php.
require_once(__DIR__.'/../../config.php');
require_once($CFG->libdir.'/adminlib.php');
include_once($CFG->dirroot . '/course/lib.php');
include_once($CFG->libdir . '/coursecatlib.php');
require_once($CFG->libdir.'/accesslib.php');


// Globals.
global $CFG, $OUTPUT, $USER, $SITE, $PAGE,$DB;

// Ensure only administrators have access.
//$homeurl = new moodle_url('/');
require_login();

if (!is_siteadmin()) {
    redirect($homeurl, "This feature is only available for site administrators.", 5);
}
$title = get_string('pluginname','block_group_notify');
$heading = get_string('heading','block_group_notify');
$url = new moodle_url('/blocks/group_notify/');

if ($CFG->branch >= 25) { // Moodle 2.5+.
    $context = context_system::instance();
} else {
    $context = get_system_context();
}

$PAGE->set_pagelayout('admin');
$PAGE->set_url($url);
$PAGE->set_context($context);
$PAGE->set_title($title);
$PAGE->set_heading($heading);
echo $OUTPUT->header();
$courses = get_courses();
$primaryadmin=get_admin();
$adminID=$primaryadmin->id;

$adminToken=$DB->get_record('external_tokens',array('userid'=> $adminID,'externalserviceid'=>'1'));

/**
* Instantiation of class
*/
$table = new html_table();
/**
* table attributes
* <table class="reportnotificationTable table table-striped generaltable">
*
*/
$table->attributes = array("class" => "groupNotifyTable table table-striped generaltable");

$table->head = array(
                    get_string('serial_number','block_group_notify'),
                    get_string('course_name','block_group_notify'),
                    get_string('send_reports','block_group_notify')
                );
static $serial_number=1;
foreach ($courses as $course) {
    $table_row = new html_table_row();
    if($course->id!=1){    
        $report_url=new moodle_url('/blocks/group_notify/group.php?id='.$course->id);
        $course = $DB->get_record('course', array('id'=> $course->id));
        
        if(empty($course->enablecompletion)){            
            $link='<a href="'.$report_url.'" class="btn btn-success" role="button" disabled>Reports</a>';
        }else{
            $link='<a href="'.$report_url.'" class="btn btn-success" role="button">Reports</a>';
        }
        $table_row->cells = array($serial_number, $course->fullname,$link);
        $table->data[] = $table_row;
        $serial_number++;
    }
}
echo html_writer::table($table);

echo $OUTPUT->footer();
