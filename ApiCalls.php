<?php

function getAdminToken($api_function_name){

    global $DB;    
    $courses = get_courses();
    $primaryadmin=get_admin();
	
    $adminID=$primaryadmin->id;
    
    try{
        try{
            $externalservice=$DB->get_record('external_services_functions',array('functionname'=>$api_function_name));
            
        }catch(Exception $e){
            throw new Exception("Api function does not have access, please check API");
        }
        $externalserviceID=$externalservice->externalserviceid;
        $adminToken=$DB->get_record('external_tokens',array('userid'=> $adminID,'externalserviceid'=>$externalserviceID));
		
    }catch(Exception $e){
        throw new Exception("Please enable webservice and manage tokens");
    }    
    return $adminToken->token;

}
/**
 * @param: Take course id from get_user_id,get_user_email method
 *
 * @return: json object of enrolled user in the course.
 */
function callApi($course_id){
    global $CFG;
    $api_function_name='core_enrol_get_enrolled_users';
    $token=getAdminToken($api_function_name);
    
    $url = "$CFG->wwwroot/webservice/rest/server.php?wstoken=$token&wsfunction=$api_function_name&moodlewsrestformat=json";
    
    $data = array('courseid' => $course_id);
    $options = array(
                'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded",
                'method'  => 'POST',
                'content' => http_build_query($data)
                ));
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    // Json Data
    return json_decode($result);
    //json data into array(object,object)
}
/**
 * *
 *
 * @param [string] $course_id
 * @return [array] groups
 */
function get_course_groups($course_id){
    global $CFG;
    $api_function_name='core_group_get_course_groups';
    $token=getAdminToken($api_function_name);
    
    $url = "$CFG->wwwroot/webservice/rest/server.php?wstoken=$token&wsfunction=$api_function_name&moodlewsrestformat=json";
    
    $data = array( 'courseid' => $course_id );
    $options = array(
                'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded",
                'method'  => 'POST',
                'content' => http_build_query($data)
                ));
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    // Json Data
    return json_decode($result);
}
/**
 * get_group_members
 *
 * @param [string] $course_id
 * @return [array] $groups and $userid
 */
function get_group_members($group_id){
    global $CFG;
    $api_function_name='core_group_get_group_members';
    $token=getAdminToken($api_function_name);
    
    $url = "$CFG->wwwroot/webservice/rest/server.php?wstoken=$token&wsfunction=$api_function_name&moodlewsrestformat=json";
    
    $data = array( 'groupids[0]' => $group_id );
    $options = array(
                'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded",
                'method'  => 'POST',
                'content' => http_build_query($data)
                ));
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    // Json Data
    return json_decode($result);
}
/**
 * @param: course_id from index.php 
 *
 * @return: All users Id who are enrolled in the course
 */
function get_user_ids($course_id){
    $resultParse=callApi($course_id); 
    foreach($resultParse as $userDetails){       
        $userIds[]=$userDetails->id;
    }
    return $userIds;
}

/**
 * @param: course_id from index.php 
 *
 * @return: All users email who are enrolled in the course
 */
function get_user_emails($course_id){
    //json data into array(object,object)
    global $DB;
    //json data into array(object,object)
    $resultParse=callApi($course_id);

        foreach($resultParse as $key=>$userDetails){  
        
            if($userDetails->enrolledcourses!=null){
                try{   
                    //$enrolIDs=$DB->get_records('enrol',array('courseid'=>$course_id,'status'=>0));                        
                        //$user_enrolment=$DB->get_record('user_enrolments',array('enrolid'=>$enrolID->id,'userid'=>$userDetails->id)); 
                        
                    $get_user_enrol_status=$DB->get_record_sql("SELECT a.status,b.enrol from {user_enrolments} as a JOIN {enrol} as b ON a.enrolid=b.id WHERE b.courseid=$course_id and a.userid=$userDetails->id");
                    
                    }catch(Exception $e){
                        throw new Exception("Unable connect to db to find status of enrolled users.");
                    }   
                    $status=$get_user_enrol_status->status;
                    
                    if(!$status)  
                        $userEmails[$userDetails->id]=$userDetails->email; 
            }
        }
    return $userEmails;
}

/**
 * @param: passing CourseId and UserID to get activities
 * 
 * Here we are calling an Api method
 * @api: core_completion_get_activities_completion_status
 * 
 * @return: All the activities which user has in the following course.
 */  
function get_activities($course_id,$userId){
    global $CFG;
    $course_id=(int)$course_id;
    $api_function_name='core_completion_get_activities_completion_status';
    $token=getAdminToken($api_function_name);
	
    try{
        /* @api : response 
        object {
        statuses   //List of activities status
        list of ( 
        //Activity
        object {
        cmid int   //comment ID
        modname string   //activity module name
		
        instance int   //instance ID
        state int   //completion state value:
                                                                            0 means incomplete, 1 complete,
                                                                            2 complete pass, 3 complete fail
        timecompleted int   //timestamp for completed activity
        tracking int   //type of tracking:
                                                                            0 means none, 1 manual, 2 automatic
        } */
        $url = "$CFG->wwwroot/webservice/rest/server.php?wstoken=$token&wsfunction=$api_function_name&courseid=$course_id&userid=$userId&moodlewsrestformat=json";
        $options = array(
        'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data)
        ));
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context); 
    }catch(Exception $e){
        throw new Exception("Api response error");
    }
    return json_decode($result);
}

/**
 * @param: UserID and CourseID
 * 
 * we are calling @method: get_activities
 * to get activities that user has in this course
 * 
 * @return: count the activity status either 
 * available or not.
 */
function get_user_activities_count($course_id,$user_id){       
    $user_activities=get_activities($course_id,$user_id);
    //return as array of objects[status,warnings]    
    // status contains objects of quiz activities
    $count_quiz_activities=array();
    foreach($user_activities as $user_activities_object){
        // array of object  
        
        foreach($user_activities_object as $user_activities_statuses){
                if($user_activities_statuses->modname==='quiz'){
                    $count_quiz_activities[]=$user_activities_statuses->modname;                
                }
        }
    } 
    
    return count($count_quiz_activities);         
}

/**
 * @param: UserID and CourseID
 * 
 * we are calling @method: get_activities
 * to get activities that user has in this course
 * 
 * @return: count the activity status either 
 * available or not.
 */
function get_user_activities_completed_count($course_id,$user_id){      
    $user_activities=get_activities($course_id,$user_id);
    
    $activity_completion_state=array();
    $quizzes_state_completed=array();
    foreach($user_activities->statuses as $single_activity_object){ 
        // from status activities object we are selecting state 
        // '1' for complete and 0 for incomplete
        $activity_state=($single_activity_object->state==1 || $single_activity_object->state==2 || $single_activity_object->state==3);
        if($activity_state && $single_activity_object->modname==='quiz' ){
            $activity_completion_state[]=$single_activity_object->state;
            $quizzes_state_completed[]=$single_activity_object->instance;
        }                
    }
    
    return count(array_filter($activity_completion_state));     
}
function get_quizzes_completed_state($course_id,$user_id){      
    $user_activities=get_activities($course_id,$user_id);    
    $quizzes_state_completed=array();
    foreach($user_activities->statuses as $single_activity_object){ 
        // from status activities object we are selecting state 
        // '1' for complete and 0 for incomplete
        $activity_state=($single_activity_object->state==1 || $single_activity_object->state==2 || $single_activity_object->state==3);
        if($activity_state && $single_activity_object->modname==='quiz' ){
            $quizzes_state_completed[]=$single_activity_object->instance;
        }                
    }
    
    return $quizzes_state_completed;     
}
function get_quizzes_completed_time($course_id,$user_id){         
    $user_activities=get_activities($course_id,$user_id);    
    $quizzes_time_completed=array();
    foreach($user_activities->statuses as $single_activity_object){ 
        // from status activities object we are selecting state 
        // '1' for complete and 0 for incomplete
        $activity_state=($single_activity_object->state==1 || $single_activity_object->state==2 || $single_activity_object->state==3);
        if($activity_state && $single_activity_object->modname==='quiz' ){
            $quizzes_time_completed[$single_activity_object->instance]=$single_activity_object->timecompleted;
        }                
    }    
    return $quizzes_time_completed;     
}
function get_user_activities_incomplete_count($course_id,$user_id){
    $user_activities=get_activities($course_id,$user_id);
    $count=0;
    foreach($user_activities->statuses as $single_activity_object){ 
        // from status activities object we are selecting state 
        // '1' for complete and 0 for incomplete
        //$activity_completion_state[]=$single_activity_object->state;       
	if($single_activity_object->state==0 && $single_activity_object->modname==='quiz'){
            //$timecompleted[]=$single_activity_object->timecompleted;
            $count++;
        }             
    }
    return $count;
}
function get_user_activities_completed_timestamp($course_id,$user_id){
    $user_activities=get_activities($course_id,$user_id);
    
    foreach($user_activities->statuses as $single_activity_object){ 
        
        // from status activities object we are selecting state 
        // '1' for complete and 0 for incomplete
        
        static $count=0;  
		$activity_state=($single_activity_object->state==1 || $single_activity_object->state==2 || $single_activity_object->state==3);
        
        if($activity_state){
            $timecompleted[]=$single_activity_object->timecompleted;
            //$count++;
        }             
    }
    return $timecompleted;
}
function get_activities_attempts_current_week_count($startTime,$endTime,Array $activity_completed_time){
    // try{
        $count=0;
        foreach($activity_completed_time as $time_completed){
        if($activity_completed_time){
            if(($time_completed > $startTime) && ($time_completed < $endTime)){
                $count++;
            }
        }
    }
   /*  }catch(Exception $e){
        throw new Exception("Unable calculate time");
    } */
    return $count;
}
function get_quizzes_attempts_current_week_count($startTime,$endTime,Array $quizAttemptTimeStampFinished){
    try{
        $count=0;
        $QuizId=array();
        foreach($quizAttemptTimeStampFinished as $key=> $time_completed)
        if(!empty($quizAttemptTimeStampFinished)){
            if(($time_completed > $startTime) && ($time_completed < $endTime)){
                
                $QuizId[]=$key;
            }
        }
    }catch(Exception $e){
        throw new Exception("Empty quiz Attempt Time Stamp");
    }
    return $QuizId;
}

/**
 * @param: Taking Activities and count Activities completed.
 * 
 * @return: Percentage completed.
 */
function course_completed_percentage($count_activities,$count_activities_completed){
    if($count_activities_completed == 0){
        return 0;
        throw new Exception('Course Incomplete');
    }else{
        $percentage_course_completed = ($count_activities_completed / $count_activities)*100;
        return $percentage_course_completed;
    }
}


/**
 * @param: passing param $couseID.
 * @api: mod_quiz_get_quizzes_by_courses
 * this @api gives us only quiz which are present 
 * in the passing argument
 * 
 * @return: All the quizzes in the course.
 */
function get_quizzes_names($course_id){
    global $CFG;
    $api_function_name='mod_quiz_get_quizzes_by_courses';
    $token=getAdminToken($api_function_name);
	    try{
        $url=$CFG->wwwroot."/webservice/rest/server.php?wstoken=$token&wsfunction=$api_function_name&moodlewsrestformat=json";
        $data = array( 'courseids[0]' => $course_id );
        $options = array(
        'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data)
        ));
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context); 
        $result_object=json_decode($result);
        $result_quizzes=$result_object->quizzes;
    }catch(Exception $e){
        throw new Exception("Api mod_quiz_get_quizzes_by_courses response error");
    }
    return $result_quizzes;
}

/**
 * @param: two arrays to match keys are equal/not.
 * 
 * @return: Boolean
 */
function keys_are_equal($array1, $array2) {
    try{
        $value=!array_diff_key($array1, $array2) && !array_diff_key($array2, $array1);
    }catch(Exception $e){
        throw new Exception("Array keys are mismatch");
    }
    return $value;
}

/**
 * @param: Taking Arrays as param 
 * quizzes sum grades and attempted quizzes grade.
 * 
 * @return: percentage of each course.
 */
function get_activity_percentage(Array $quizSumGrades,Array $quizAttemptGrades,Array $quizNames,Array $quizState){
//check whether keys are equal

    try{
        if(keys_are_equal($quizSumGrades,$quizAttemptGrades)){
            /* returns an array of the keys listed in the array */
            $keys = array_keys($quizAttemptGrades);
            foreach($keys as $key ){
                try{               
                    
                    if($quizSumGrades[$key]==0) {
                        // echo "please Add Question in following Quiz";
                        echo 'Please Add Question to following Quiz'.$quizNames[$key];
                        //throw new Exception("Division by zero is invalid<br/>");
                    }/* I'm not sure which way you're actually dividing the values, so feel free to change as necessary */
                }catch(Exception $e){
                    echo $e->getMessage();
                }
                if($quizAttemptGrades[$key]==null){
                    $the_calculated_percentage[$key]="Not Attempted";
                }else{
                    
                    if(in_array($key,$quizState)){                        
                        $the_calculated_percentage[$key]=round(($quizAttemptGrades[$key]/$quizSumGrades[$key])*100);
                    }else{
                    $the_calculated_percentage[$key]="Not Attempted";
                    }
                }
            }
        }   
    }catch(Exception $e){
        throw new Exception('Keys are not equal ');
    }
    return $the_calculated_percentage;
}
