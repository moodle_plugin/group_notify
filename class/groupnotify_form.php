<?php
// This file is part of eMailTest plugin for Moodle - http://moodle.org/
//
// eMailTest is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// eMailTest is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with eMailTest.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Main form for eMailTest.
 *
 * @package    block_reportnotification
 * @copyright  2015-2017 TNG Consulting Inc. - www.tngcosulting.ca
 * @author     Michael Milette
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;
require_once($CFG->libdir.'/formslib.php');
require_once($CFG->dirroot . '/enrol/externallib.php');

/**
 * Form to prompt administrator for the recipient's email address.
 * @copyright  2015-2017 TNG Consulting Inc. - www.tngcosulting.ca
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class groupnotify_form extends moodleform {

    /**
     * Define the form.
     */
    public function definition() {
        global $USER, $CFG,$DB;
        $mform = $this->_form;
     
            
            //send in smtp method
            $sendmethod = get_string('smtpmethod', 'block_group_notify', $CFG->smtphosts);
        
        if ($CFG->branch >= 32) {
            
            //moodle version is above 3.2
            
            $sendmethod .= ' (<a href="../../admin/settings.php?section=outgoingmailconfig#admin-smtphosts">' .
                    get_string('change', 'admin').'</a>)';
        } else {
            $sendmethod .= ' (<a href="../../admin/settings.php?section=messagesettingemail">' .
                    get_string('change', 'admin').'</a>)';
        }
     

        // Sender.

        $senderarray = array();
        $recipientarray=array();
        $a = new stdClass();
        $a->label = get_string('change', 'admin');

        
        // Support email address.
        $primaryadmin = get_admin();
        $a->email = empty($CFG->supportemail) ? $primaryadmin->email : $CFG->supportemail;
        $a->url = '../../admin/settings.php?section=supportcontact';
        $a->type = get_string('supportemail', 'admin');
        $senderarray[] = $mform->createElement('radio', 'sender', '', get_string('from', 'block_group_notify', $a), $a->email);
        if (!validate_email($a->email)) {
            $senderarray[] = $mform->CreateElement('static', 'error', '',
                    html_writer::span(get_string('invalidemail'), 'statuswarning'));
        }

  
        $course_id=$_GET['id'];
      
        $userEmails=array();   

        $url = "$CFG->wwwroot/webservice/rest/server.php?wstoken=718e4ee5732cf9393401d9095e97e9a8&wsfunction=core_enrol_get_enrolled_users&moodlewsrestformat=json";
        $data = array( 'courseid' => $course_id );

        $options = array(
        'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded",
        'method'  => 'POST',
        'content' => http_build_query($data)
        )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        // Json Data
        
        //json data into array(object,object)
        $resultParse=json_decode($result);       
       
        
        foreach($resultParse as $userDetails){       
            $userEmails[]=$userDetails->email;    
                    
       }
       
    
        //seperating emails with comma
        $email_combined=implode(',',$userEmails);
        /**
        * Adding this emails into value 
        * Emails display by seperating with comma.
        */
        // Recipient.
        $mform->addElement('text', 'recipient',get_string('toemail', 'block_reportnotification'), 'enable maxlength="100" size="25" value="'.$email_combined.'" ');

        
        
        // Add group of sender radio buttons to form.
        $mform->setDefault('sender', $this->_customdata['fromdefault']);
        $mform->addGroup($senderarray, 'senderar', get_string('fromemail', 'block_reportnotification'), array('<br />'), false);
        $mform->setType('sender', PARAM_EMAIL);
        $mform->addRule('senderar', get_string('required'), 'required');

      
       /*   $value="Ageing asia";
        $mform->addElement('textarea','message', get_string('messagetext', 'block_reportnotification'),
                            array('wrap'=> virtual, 'rows' => 6, 'cols' => 60));
       
         $mform->setType('message', PARAM_TEXT);*/

        global $USER,$DB,$CFG;

            $conn = new mysqli($CFG->dbhost, $CFG->dbuser,$CFG->dbpass,$CFG->dbname);

            if ($conn->connect_error) {
                             die("Connection failed: " . $conn->connect_error);
                                          }
              
             $query_cname= "SELECT c.fullname,ct.name
                            FROM mdl_course AS c
                            JOIN mdl_course_categories AS ct ON c.category=ct.id
                            WHERE c.id=$course_id";

            $result_cname= $conn->query($query_cname);

            $cdrow=mysqli_fetch_array($result_cname,MYSQLI_NUM);

            $course_name = $cdrow[0];

            $category_name = $cdrow[1];

        
           
/*        $coursename=new stdClass();
      $coursename = $DB->get_record('course', array('id' => $course->id));
        $couse_name=$cousename->fullname;*/
        //$course_name=$coursecontext->get_formatted_name();
        //$categoryname = $category->get_formatted_name();
        
        $defaulttext="
        <div><h1><strong>Hi admin</strong></h1>
        <ul>
        <li><strong>course id:</strong>$course_id</li>
        <li><strong>course name:</strong>$course_name</li>
        <li><strong>course category:</strong>$category_name</li>
        </li>
        </ul>
        </div>";
      //  $mform->addElement('text', 'desc',get_string('description'));
       // $mform->addElement('editor', 'desc', get_string('description'));     
      // $mform->addElement('textarea', 'desc', get_string('description'),'arshad','syed');   
       $mform->addElement('textarea', 'desc', get_string("introtext", "survey"), 'wrap="virtual" rows="20" cols="50"'); 
        
       // $mform->setDefault('desc', array('text'=>$defaulttext));
         $mform->setDefault('desc',$defaulttext);
        // Buttons.

        $buttonarray = array();
        $buttonarray[] = $mform->createElement('submit', 'send', get_string('sendtest', 'block_reportnotification'));
        $buttonarray[] = $mform->createElement('cancel');

        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->closeHeaderBefore('buttonar');
    }
     /**
     * Return course_id if a cancel button has been pressed resulting in the form being submitted.
     *
     * @return value course_id if a cancel button has been pressed
     */
    function is_cancelled(){

        $mform =& $this->_form;
        if ($mform->isSubmitted()){
            foreach ($mform->_cancelButtons as $cancelbutton){
                if (optional_param($cancelbutton, 0, PARAM_RAW)){
                    return true;
                }
            }
        }
        return false;
    }

    
    /**
     * Validate submitted form data, recipient in this case, and returns list of errors if it fails.
     *
     * @param      array  $data   The data fields submitted from the form.
     * @param      array  $files  Files submitted from the form (not used)
     *
     * @return     array  List of errors to be displayed on the form if validation fails.
     */
    public function validation($data, $files) {
        $errors = parent::validation($data, $files);
 

    if (empty($data['recipient'])) {
            $errors['recipient'] = get_string('err_email', 'form');
        }

        return $errors;
    }
}


