<?php

class block_group_notify extends block_base{
    function init(){
        $this->title=get_string('pluginname','block_group_notify');
        $this->version=2016052300;
        }

    /**
    * Creates the blocks main content
    *
    * @return string
    */
    function get_content() { 
        global $CFG;      
        if ($this->content !== null) {
            return $this->content;
        }       
        $this->content = new stdClass;
        // Guests do not have any progress. Don't show them the block.
        if (!isloggedin() or isguestuser()) {
            return $this->content;
        }

        $this->content->footer = '<br /><a href="'.$CFG->wwwroot.'/blocks/group_notify/redirect.php" class="btn btn-primary" role="button">';
        $this->content->footer .= get_string('groupNotify','block_group_notify').'</a>'; 
        return $this->content;      
    }
    /**
     * Defines where the block can be added
     *
     * @return array
     */
    public function applicable_formats() {
        return array(
            'site'           => false,
            'my'             => true
        );
    }

    /**
     * Allow block instance configuration
     *
     * @return bool
     */
    public function has_config() {
        return true;
    }   

}//class block_group_notify
