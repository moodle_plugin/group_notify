<?php
// This file is part of MailTest for Moodle - http://moodle.org/
//
// MailTest is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// MailTest is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with MailTest.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays the form and processes the form submission.
 *
 * @package    local_mailtest
 * @copyright  2015-2017 TNG Consulting Inc. - www.tngconsulting.ca
 * @author     Michael Milette
 * @license    http://www.gnu.org/copyleft/gpl.html  GNU GPL v3 or later
 */

    // Include config.php.
    require_once(__DIR__.'/../../config.php');
    require_once($CFG->libdir.'/adminlib.php');
    require_once($CFG->libdir.'/accesslib.php');
    require_once($CFG->dirroot.'/group/externallib.php');
    require_once($CFG->dirroot.'/blocks/group_notify/locallib.php');
    require_once($CFG->dirroot.'/blocks/group_notify/ApiCalls.php');

   

    // Globals.
    global $CFG, $OUTPUT, $USER, $SITE, $PAGE,$DB;

    // Ensure only administrators have access.
    $homeurl = new moodle_url('/');
    require_login();
    if (!is_siteadmin()) {
        redirect($homeurl, "This feature is only available for site administrators.", 5);
    }
    // Include form.
    require_once(dirname(__FILE__).'/class/groupnotify_form.php');
    
    // Get ID from url
    $course_id=$_GET['id'];
    $title = get_string('pluginname','block_group_notify');
    $heading = get_string('heading','block_group_notify');
    $url = new moodle_url('/blocks/group_notify/redirect.php');
     /**
     * get_record is Data manipulation in Moodle
     * You can refer more in below link
     * @url: https://docs.moodle.org/dev/Data_manipulation_API
     * 
     */
    $course = $DB->get_record('course', array('id'=> $course_id));
    $course_name=$course->fullname;
	$course_short_name=$course->shortname;
    $group_ids=array();
    /**
     * core_group_external::get_course_groups($course_id);
     * @param: courseID
     * 
     * @return: array(array(with group details: Id,name.......));
     * 
     */
    $groups=core_group_external::get_course_groups($course_id);
    
    foreach ($groups as $group) {
        $group_id=null;
        // Collecting all the group ids.
        $group_ids[]=$group['id'];
        $group_id=$group['id'];
        $groups_name[$group_id]=$group['name'];
    }
    
    
    /**
     * core_group_external::get_group_members($group_ids);
     * @param: array of group ids
     * 
     * @return: array(array(groupid,userids(array)))
     */
    $groupAndMemberIds=core_group_external::get_group_members($group_ids);
    
    
    
    /**
     * get_context_instance(CONTEXT_COURSE,$course_id);
     * Moodle check course has permission by passing course_id. 
     * 
     */
    $context_course = get_context_instance(CONTEXT_COURSE,$course_id);   
    
    /**
     * get_record is Data manipulation in Moodle
     * You can refer more in below link
     * @url: https://docs.moodle.org/dev/Data_manipulation_API
     * 
     */
    $course = $DB->get_record('course', array('id'=> $course_id));
    $coursepage_url=new moodle_url('/course/view.php?id='.$course->id);
    if ($CFG->branch >= 25) { // Moodle 2.5+.
        $context = context_system::instance();
    } else {
        $context = get_system_context();
    }
    $PAGE->set_pagelayout('admin');
    $PAGE->set_url($url);
    $PAGE->set_context($context);
    $PAGE->set_title($title);
    $PAGE->set_heading($heading);

    $CFG->noreplyaddress = empty($CFG->noreplyaddress) ? 'noreply@' . get_host_from_url($CFG->wwwroot) : $CFG->noreplyaddress;

    if (!empty($CFG->emailonlyfromnoreplyaddress) || $CFG->branch >= 32) { // Always send from no reply address.
        // Use primary administrator's name if support name has not been configured.
        $primaryadmin = get_admin();
        $CFG->supportname = empty($CFG->supportname) ? fullname($primaryadmin, true) : $CFG->supportname;
        // Use noreply address.
        $fromemail = local_mailtest_generate_email_user($CFG->noreplyaddress, format_string($CFG->supportname));
        $fromdefault = $CFG->noreplyaddress;
    } else { // Otherwise defaults to send from primary admin user.
        $fromemail = get_admin();
        $fromdefault = $fromemail->email;
    }
    /**
     * getting userids from course.
     */
    $user_ids=get_user_ids($course_id);
    
    /**
     * Users emails in course
     */
    $user_emails=get_user_emails($course_id);
    
    /**
     * return value is quizzes which are present in the course.
     */
    $course_quizzes=get_quizzes_names($course_id);
    
    /**
     * Getting quiz name from database
     * Using courseId which we get from url
     * every time we passing quizID, Only quiz id 
     * 
     * @return mixed: $quiz get all details of quiz.
     * sumgrades and quiz name
     */
    $quizSumGrades=array();
    foreach($course_quizzes as $course_quiz_name){
    $quiz = $DB->get_record('quiz', array('id' =>$course_quiz_name->id,'course'=>$course_id));
    $quizSumGrades[$course_quiz_name->id]=$quiz->sumgrades;
    $quizNames[$course_quiz_name->id]=$quiz->name;
    } 
    

    echo $OUTPUT->header();
    echo $OUTPUT->heading($heading);
	
    if(empty($groups) || is_null($course_quizzes) || is_null($user_ids) ){
        echo "<h4> To get Group report you should have Quizzes, Users and Groups in the Courses </h4>";
    }else{
	

    foreach ($groupAndMemberIds as $groupIdUsersId) {
        
		$get_group_id=$groupIdUsersId['groupid'];
        $group_name=$groups_name[$get_group_id];
		
        // single group users ids.
        $groupUserIds=$groupIdUsersId['userids'];
        $usersRoleDetails=array();
        //From userIds and getting roles of single user.
        foreach($groupUserIds as $groupUserId){
            /**
             * get_user_roles($context_course,$userId);
             * @param: $context_course(220), 
             * @param: userID
             * 
             * @return mixed: roleid,contextid,userid.
             */
            
            $usersRoleDetails[]=get_user_roles($context_course,$groupUserId);
        }
        $teacherDetails=$studentDetails=$teacherID=$teacherIdStored=Null;
        $currentWeekPercentage=0;
                    
        $quizAttemptTimeStampFinishedchecking=$teacherEmails=$studentIds=$studentsFirstname=$teachersFirstname=$count_activities=$count_activities_completed=$resultCourseCompletedPercentage=$currentWeekPercentage=$studentsLastname=$activities_completed_time=array();
        /**
         * $usersRoleDetails is containing array(array(object))
         * 
         * $userIdDetails contains all details of the user in the form of (object{id,roleid,shortname})
         */
        foreach($usersRoleDetails as $key=>$userIdDetails){
            /**
             * $userIdDetails containing array(userid => object{id,shortname,roleid})
             * we filtering an array
             */            
            foreach($userIdDetails as $key=>$userDetailsObject){            
                $userRoleID = $userDetailsObject->roleid;                
                $endTime=time();
				$date = date('d-m-Y',$endTime);
				$startTime=$endTime-(86400*7);
								
                //Editing Teacher, Not Editing Teacher and Student
                if($userRoleID=="3" || $userRoleID=="4"){
                        /**
                         * Getting Emails from database 
                         * @param  $id=>teacher role ids
                         * database table : user
                         * 
                         * @return : userdetails
                         */
                        $teacherID=$userDetailsObject->userid;
                        if($teacherIdStored != $teacherID){
                            $teacherIdStored=$teacherID;
                            $teacherDetails = $DB->get_record('user', array('id' => $teacherID));
                        }
                        $teacherEmails[$userDetailsObject->userid]=$teacherDetails->email;
                        $teachersFirstname[$userDetailsObject->userid]=$teacherDetails->firstname; 
						$teachersLastname[$userDetailsObject->userid]=$teacherDetails->lastname;
                }//if condition 
                
                if($userRoleID=="5"){
                    $studentIds[]=$userDetailsObject->userid;
                    $studentID=$userDetailsObject->userid;
                    $get_user_enrol_status=$DB->get_record_sql("SELECT a.status,b.enrol from {user_enrolments} as a JOIN {enrol} as b ON a.enrolid=b.id WHERE b.courseid=$course_id and a.userid=$studentID");
                    $status=$get_user_enrol_status->status;                    
                    if(!$status){                        
                    $studentDetails = $DB->get_record('user', array('id' => $studentID));                    
                    $studentsFirstname[$studentID]=$studentDetails->firstname;
					$studentsLastname[$studentID]=$studentDetails->lastname;
                    $count_activities[$studentID]=get_user_activities_count($course_id,$studentID);
                    $count_activities_completed[$studentID]=get_user_activities_completed_count($course_id,$studentID);
					
                    try{
                        /**
                         * Overall percentage of student.
                         */
                        $CourseCompletedPercentage=course_completed_percentage($count_activities[$studentID], $count_activities_completed[$studentID]);
                        $resultCourseCompletedPercentage[$studentID]=round($CourseCompletedPercentage);
                    }catch(Exception $e){
                        echo 'Caught exception: ',  $e->getMessage(), "\n";
                    }

                    $quizAttemptGrades=array();
                    $quizAttemptTimeStampFinished=array();
                    $quizAttemptTimeFinished=get_quizzes_completed_time($course_id,$studentID);
                    
					
					
                    foreach($course_quizzes as $course_quiz_name){
                        $courseQuizID=(string)$course_quiz_name->id;
                        $studentID=(string)$studentID;
                        $quizAttempt=$DB->get_record_sql('SELECT * FROM {quiz_attempts} WHERE quiz = ? AND userid = ? ORDER BY timemodified DESC', array( $courseQuizID ,$studentID ));
						
                        if(($quizAttempt->attempt > 1) && !is_null($quizAttempt->sumgrades)){
                            $quizAttemptGrades[$course_quiz_name->id]=$quizAttempt->sumgrades;
                        }else{
                                $quizAttempt=$DB->get_record('quiz_attempts',array('quiz'=>$courseQuizID,'userid'=> $studentID));
                                $quizAttemptGrades[$course_quiz_name->id]=$quizAttempt->sumgrades;
                        }       
                        $quizAttemptTimeStampFinished[$courseQuizID]=$quizAttemptTimeFinished[$courseQuizID];         
                    } 
					
                    try{
                            $quizState=get_quizzes_completed_state($course_id,$studentID);
                    }catch(Exception $e){
                            throw new Exception("Quizzes Status Not Available!");
                    } 
					
                    $lastWeekIncompleteQuizzesStatuses=0;
                    $lastWeekIncompleteQuizzesStatuses=get_user_activities_incomplete_count($course_id,$studentID);
                    
                    $activities_completed_time[$studentID]=get_user_activities_completed_timestamp($course_id,$studentID);
					if(is_null($activities_completed_time[$studentID])){
                        $activities_completed_time[$studentID]=array();
                    }
                    
                    $currentWeekActivityCompletedCount=0;
                    try{
                        if(!empty($count_activities_completed)){
                            $currentWeekActivityCompletedCount=get_activities_attempts_current_week_count($startTime,$endTime,$activities_completed_time[$studentID]);
                        } 
                    }catch(Exception $e){
                        
                        $currentWeekActivityCompletedCount=0;
                        //throw new Exception("Empty current week activitiy Completed count");
                    }
					
                    $lastWeekIncompleteActivities=0;
                    $lastWeekIncompleteActivities=$lastWeekIncompleteQuizzesStatuses+$currentWeekActivityCompletedCount;

                    
                    $quizAttemptTimeStampFinishedchecking=$quizAttemptTimeStampFinished;
                    rsort($quizAttemptTimeStampFinishedchecking);
					
                    foreach($quizAttemptTimeStampFinishedchecking as $quizSingleAttemptTimeStampFinishedchecking){
                        if(!is_null($quizSingleAttemptTimeStampFinishedchecking)){
                            if($startTime<$quizSingleAttemptTimeStampFinishedchecking){
								
                                $WeekPercentage=($currentWeekActivityCompletedCount/$count_activities[$studentID])*100;
                                $currentWeekPercentage[$studentID]=round($WeekPercentage);
                            }else{
                                $WeekPercentage=($currentWeekActivityCompletedCount/$lastWeekIncompleteActivities)*100;
                                $currentWeekPercentage[$studentID]=round($WeekPercentage);
                            }
                        }                        
                    }//foreach $quizAttemptTimeStampFinishedchecking   
					
                    if(is_null($currentWeekPercentage[$studentID])){
                        $currentWeekPercentage[$studentID]=0;
                    } 
                
                }// if student is active
                }//if student role
            
            }//foreach userIdDetails 
        }//foreach usersRoleDetails
        
        
            
			if(empty($teacherEmails) || empty($studentsFirstname)){
                echo "<h4>No user is enrolled as student and teacher in the group $group_name</h4>";
            }


            
            // Send test email.
            $fromemail = local_mailtest_generate_email_user($fromemail->email,'');   
            
            /**
             * sending email to multiple user
             * arshads@3esofttech.com,engg.arshad83@gmail.com
             */  
            foreach($teacherEmails as $key=>$teacherEmail){
                $toemail = local_mailtest_generate_email_user($teacherEmail, '');
				$teacherFirstName=$teachersFirstname[$key];
                $teacherLastName=$teachersLastname[$key];
                $subject_message="$teacherFirstName $teacherLastName, Your Group's $course_short_name course progress report";
                $subject =format_string($subject_message);
                // Add some system information.
                $a = new stdClass();
                if (isloggedin()) {
                    $a->regstatus = get_string('registered','block_group_notify', $USER->username);
                } else {
                    $a->regstatus = get_string('notregistered','block_group_notify');
                }
                
                $message='
                <div class="container" style="font-family:Arial;font-size:14px;">
                    <div style="text-align:center">
                    <img src="https://engineers.academy/wp-content/uploads/2017/03/Eng_ac_logo_final_smal.png" alt="engineersAcademy">
                    </div>
                   <p>Hi '.$teachersFirstname[$key].',</p>                  
                    
                    <p class="course-date">This weekly report is for the following Engineers Academy unit / course, for the week ending <span class="course-date">'. $date.' :</span></p>
                    <p>'.$course_name.'</p>
                    <p>Your students\' weekly and overall progress has been summarised in the table below:</p>';   
				if(empty($studentsFirstname)){
                   $message.="<h4> No Students in your Group: $group_name</h4>"; 
                }else{
                $message.='<table  style="font-weight:400;
                font-family: Arial;
                border: 1px solid black;
                width: 100%;">
                <thead>
                    <tr>
                        <th style="border: 1px solid #dddddd;
                        text-align: left;
                        padding: 8px;
                        font-weight:800;
                        background-color:black;
                        color:white;">Name</th>
                        <th style="border: 1px solid #dddddd;
                        text-align: left;
                        padding: 8px;
                        font-weight:800;
                        background-color:black;
                        color:white;">Weekly Progress</th> 
                        <th style="border: 1px solid #dddddd;
                        text-align: left;
                        padding: 8px;
                        font-weight:800;
                        background-color:black;
                        color:white;">Overall Progress</th>
                    </tr>
                </thead>';
                foreach($studentsFirstname as $key=>$studentFirstname){                    
                $message.='<tr>
                        <td style="border: 1px solid #dddddd;
                        text-align: left;
                        padding: 8px;
                        ">'.$studentsFirstname[$key].' '.$studentsLastname[$key].'</td>
                        <td style="border: 1px solid #dddddd;
                        text-align: left;
                        padding: 8px;
                        ">'.$currentWeekPercentage[$key].'%</td>
                        <td style="border: 1px solid #dddddd;
                        text-align: left;
                        padding: 8px;
                        ">'.$resultCourseCompletedPercentage[$key].'%</td>
                        
                        </tr>';
                }
				}
                $message.='</table>
                </div>
                <br>
                <div class="footer" style="font-family:Arial;font-size:14px;">    
                <b><i>Disclaimer:</i></b>
                
                <span><i>You have received this email because you are appointed as a manager or mentor
                for a group of students enrolled on a unit / course with the Engineers Academy. If you no longer wish to receive weekly reports for your group, then please email us on <strong>weeklyreports@engineers.academy</strong> to request removal from the reporting schedule.</i>  
                </span>
                </div>';
    
                $messagetext=html_to_text($message);
                // Manage Moodle SMTP debugging display.
                $debuglevel = $CFG->debug;
                $debugdisplay = $CFG->debugdisplay;
                $debugsmtp = $CFG->debugsmtp;
                $showlog = !empty($data->alwaysshowlog) || ($debugdisplay && $debugsmtp);
                // Set debug level to a minimum of NORMAL: Show errors, warnings and notices.
                if ($CFG->debug < 15) {
                    $CFG->debug = 15;
                }
                $CFG->debugdisplay = true;
                $CFG->debugsmtp = true;
                ob_start(); 
                $success = email_to_user($toemail, $fromemail, $subject, $messagetext,  $message, '', '', true);  

                $smtplog = ob_get_contents();
                ob_end_clean();
                $CFG->debug = $debuglevel;
                $CFG->debugdisplay = $debugdisplay;
                $CFG->debugsmtp = $debugsmtp;  
            }//foreach template
            
    }//single group
    
    
    if ($success){ // Success.

        if ($showlog) {
            // Display debugging info if settings were already on before the test or user wants to force display.
            echo $smtplog;
        }
        if (empty($CFG->smtphosts)) {
            $msg = get_string('sentmailphp','block_group_notify');
        } else {
            $msg = get_string('sentmail','block_group_notify');
        }
        local_mailtest_msgbox($msg, get_string('success'), 2, 'infobox', $url);

    } else { // Failed to deliver message to the SMTP mail server.

    if (trim($smtplog) == false) { // No communication between Moodle and the SMTP server.
        $errstring = 'errorcommunications';
    } else { // SMTP mail server refused the email.
        $errstring = 'errorsend';
        // Display the results of the dialogue between Moodle and the SMTP server.
        echo $smtplog;
    }

    if ($CFG->branch >= 32) {
        $msg = get_string($errstring,'block_group_notify', '../../admin/settings.php?section=outgoingmailconfig');
    } else {
        $msg = get_string($errstring,'block_group_notify', '../../admin/settings.php?section=messagesettingemail');
    }
    local_mailtest_msgbox($msg, get_string('emailfail', 'error'), 2, 'errorbox', $url);

    }

}//else
// Footing  =========================================================.
echo $OUTPUT->footer();
