<?php
    namespace block_group_notify\task;
   
    class studentsreport extends \core\task\scheduled_task{      
        public function get_name() {
            // Shown in admin screens
            return get_string('studentsreport','block_group_notify');
        }                                                                     
        public function execute() {    
   			global $CFG;
            echo 'Report Notification Scheduling Task'.PHP_EOL; 
            require_once($CFG->dirroot.'/blocks/group_notify/classes/Course.php');
            require_once($CFG->dirroot.'/blocks/group_notify/classes/Email2.php');                       
            $courses=get_all_courses();
            echo "Email----sending-------starts".PHP_EOL;
            foreach($courses as $course){
                $success=send_courseId_to_emails($course['id'],$course['name']);
                echo "message successfully send to ".$course['name'].$success.PHP_EOL; 
            }
            echo "Email----sending-----ends";
        }                                                                                                                               
    } 
