<?php

require_once($CFG->dirroot.'/blocks/group_notify/ApiCalls.php');
require_once($CFG->dirroot.'/blocks/group_notify/locallib.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/accesslib.php');


function send_courseId_to_emails($course_id,$course_name){

    global $CFG, $OUTPUT,$DB,$USER, $SITE, $PAGE;
    
     /**
     * get_record is Data manipulation in Moodle
     * You can refer more in below link
     * @url: https://docs.moodle.org/dev/Data_manipulation_API
     * 
     */
    $course = $DB->get_record('course', array('id'=> $course_id));
    $course_short_name=$course->shortname;    
    $group_ids=array();
    /**
     * get_course_groups($course_id);
     * @param: courseID
     * 
     * @return: array(object{with group details: Id,name.......});
     * 
     */
    
    $groups=get_course_groups($course_id);
    	
    foreach ($groups as $key=>$group) {
        // Collecting all the group ids.
        $group_ids[$group->id]=$group->id;
    }
    
    /**
     * get_group_members($group_ids);
     * @param: array of group ids
     * 
     * @return: array(object{groupid,userids(array(ids))})
     */
    $groupAndMemberIds=array();
    
    foreach ($groups as $group) {
        // Collecting all the group ids.
        $groupAndMemberIds[]=get_group_members($group->id);    
    }
    
    /**
     * get_context_instance(CONTEXT_COURSE,$course_id);
     * Moodle check course has permission by passing course_id. 
     * 
     */
    $context_course = get_context_instance(CONTEXT_COURSE,$course_id);   
    
    /**
     * get_record is Data manipulation in Moodle
     * You can refer more in below link
     * @url: https://docs.moodle.org/dev/Data_manipulation_API
     * 
     */


    $CFG->noreplyaddress = empty($CFG->noreplyaddress) ? 'noreply@' . get_host_from_url($CFG->wwwroot) : $CFG->noreplyaddress;

    if (!empty($CFG->emailonlyfromnoreplyaddress) || $CFG->branch >= 32) { // Always send from no reply address.
        // Use primary administrator's name if support name has not been configured.
        $primaryadmin = get_admin();
        $CFG->supportname = empty($CFG->supportname) ? fullname($primaryadmin, true) : $CFG->supportname;
        // Use noreply address.
        $fromemail = local_mailtest_generate_email_user($CFG->noreplyaddress, format_string($CFG->supportname));
        $fromdefault = $CFG->noreplyaddress;
    } else { // Otherwise defaults to send from primary admin user.
        $fromemail = get_admin();
        $fromdefault = $fromemail->email;
    }
    /**
     * getting userids from course.
     */
    $user_ids=get_user_ids($course_id);
    /**
     * Users emails in course
     */
    /**
     * return value is quizzes which are present in the course.
     */
    $course_quizzes=get_quizzes_names($course_id);

    /**
     * Getting quiz name from database
     * Using courseId which we get from url
     * every time we passing quizID, Only quiz id 
     * 
     * @return mixed: $quiz get all details of quiz.
     * sumgrades and quiz name
     */
    $quizSumGrades=array();
    foreach($course_quizzes as $course_quiz_name){
    $quiz = $DB->get_record('quiz', array('id' =>$course_quiz_name->id,'course'=>$course_id));
    $quizSumGrades[$course_quiz_name->id]=$quiz->sumgrades;
    $quizNames[$course_quiz_name->id]=$quiz->name;
    } 


    if(empty($groups) || empty($course_quizzes)|| is_null($user_ids) ){
        echo "<h4> To get Group report you should have Quizzes, Users and Groups in the $course_id </h4>";
    }else{
    /**
     * $groupAndMemberIds is array contains
     * collection of arrays under each array
     * collection of objects with groupid and userids
     * again userids are array.
     */
    foreach ($groupAndMemberIds as $groupAndMemberIds) {}

        foreach($groupAndMemberIds as $groupIdUsersId ){
        // single group users ids.
        $groupUserIds=$groupIdUsersId->userids;
        $usersRoleDetails=array();
        //From userIds and getting roles of single user.
            foreach($groupUserIds as $groupUserId){
                /**
                 * get_user_roles($context_course,$userId);
                 * @param: $context_course(220), 
                 * @param: userID
                 * 
                 * @return mixed: roleid,contextid,userid.
                 */
                $usersRoleDetails[]=get_user_roles($context_course,$groupUserId);
            }
        
        $teacherDetails=$studentDetails=$teacherID=$teacherIdStored=Null;
        $currentWeekPercentage=0;
                    
        $quizAttemptTimeStampFinishedchecking=$teacherEmails=$studentIds=$studentsFirstname=
        $teachersFirstname=$count_activities=$count_activities_completed=
        $resultCourseCompletedPercentage=$currentWeekPercentage=$studentsLastname=$activities_completed_time=array();
        /**
         * $usersRoleDetails is containing array(array(object))
         * 
         * $userIdDetails contains all details of the user in the form of array((object{id,roleid,shortname}))
         */
        foreach($usersRoleDetails as $key=>$userIdDetails){
            /**
             * $userIdDetails containing array(userid => object{id,shortname,roleid})
             * we filtering an array
             */

            foreach($userIdDetails as $key=>$userDetailsObject){
            
                $userRoleID = $userDetailsObject->roleid;
                
                $endTime=time();
                $date = date('d-m-Y',$endTime);
                
                $startTime=$endTime-(86400*7);
                
                if($userRoleID=="3" || $userRoleID=="4"){
                        /**
                         * Getting Emails from database 
                         * @param  $id=>teacher role ids
                         * database table : user
                         * 
                         * @return : userdetails
                         */
                        $teacherID=$userDetailsObject->userid;
                        
                        if($teacherIdStored != $teacherID){
                            $teacherIdStored=$teacherID;
                            $teacherDetails = $DB->get_record('user', array('id' => $teacherID));
                        
                        }
                        $teacherEmails[$userDetailsObject->userid]=$teacherDetails->email;
                        $teachersFirstname[$userDetailsObject->userid]=$teacherDetails->firstname; 
                        $teachersLastname[$userDetailsObject->userid]=$teacherDetails->lastname;
                        
                }//if condition 
                
                elseif($userRoleID=="5"){
                    $studentIds[]=$userDetailsObject->userid;
                    $studentID=$userDetailsObject->userid;
                    $get_user_enrol_status=$DB->get_record_sql("SELECT a.status,b.enrol from {user_enrolments} as a JOIN {enrol} as b ON a.enrolid=b.id WHERE b.courseid=$course_id and a.userid=$studentID");
                    $status=$get_user_enrol_status->status;
                        
                    if(!$status){
                    $studentDetails = $DB->get_record('user', array('id' => $studentID));
                    $studentsFirstname[$studentID]=$studentDetails->firstname;
                    $studentsLastname[$studentID]=$studentDetails->lastname;
                    $count_activities[$studentID]=get_user_activities_count($course_id,$studentID);
                    $count_activities_completed[$studentID]=get_user_activities_completed_count($course_id,$studentID);
                    try{
                        /**
                         * Overall percentage of student.
                         */
                        $CourseCompletedPercentage=course_completed_percentage($count_activities[$studentID], $count_activities_completed[$studentID]);
                        $resultCourseCompletedPercentage[$studentID]=round($CourseCompletedPercentage);
                        //var_dump($resultCourseCompletedPercentage[$studentID]);
                    }catch(Exception $e){
                        echo 'Caught exception: ',  $e->getMessage(), "\n";
                    }

                    $quizAttemptGrades=array();
                    $quizAttemptTimeStampFinished=array();
                    $quizAttemptTimeFinished=get_quizzes_completed_time($course_id,$studentID);
                    
                    foreach($course_quizzes as $course_quiz_name){
                        $courseQuizID=(string)$course_quiz_name->id;
                        $studentID=(string)$studentID;
                        $quizAttempt=$DB->get_record_sql('SELECT * FROM {quiz_attempts} WHERE quiz = ? AND userid = ? ORDER BY timemodified DESC', array( $courseQuizID ,$studentID ));
                        if(($quizAttempt->attempt > 1) && !is_null($quizAttempt->sumgrades)){
                            $quizAttemptGrades[$course_quiz_name->id]=$quizAttempt->sumgrades;
                        }else{
                                $quizAttempt=$DB->get_record('quiz_attempts',array('quiz'=>$courseQuizID,'userid'=> $studentID));
                                $quizAttemptGrades[$course_quiz_name->id]=$quizAttempt->sumgrades;
                        }       
                        $quizAttemptTimeStampFinished[$courseQuizID]=$quizAttemptTimeFinished[$courseQuizID];         
                    }    
                    try{
                            $quizState=get_quizzes_completed_state($course_id,$studentID);
                    }catch(Exception $e){
                            throw new Exception("Quizzes Status Not Available!");
                    } 
                    $lastWeekIncompleteQuizzesStatuses=0;
                    $lastWeekIncompleteQuizzesStatuses=get_user_activities_incomplete_count($course_id,$studentID);
                    
                    $activities_completed_time[$studentID]=get_user_activities_completed_timestamp($course_id,$studentID);
                    if(is_null($activities_completed_time[$studentID])){
                        $activities_completed_time[$studentID]=array();
                    }
                    $currentWeekActivityCompletedCount=0;
                    try{
                        if(!empty($count_activities_completed)){
                            $currentWeekActivityCompletedCount=get_activities_attempts_current_week_count($startTime,$endTime,$activities_completed_time[$studentID]);
                        }
                    }catch(Exception $e){
                        
                        $currentWeekActivityCompletedCount=0;
                    }
                    $lastWeekIncompleteActivities=0;
                    $lastWeekIncompleteActivities=$lastWeekIncompleteQuizzesStatuses+$currentWeekActivityCompletedCount; 
                    
                    $quizAttemptTimeStampFinishedchecking=$quizAttemptTimeStampFinished;
                    rsort($quizAttemptTimeStampFinishedchecking);
                    foreach($quizAttemptTimeStampFinishedchecking as $quizSingleAttemptTimeStampFinishedchecking){
                        if(!is_null($quizSingleAttemptTimeStampFinishedchecking)){
                            if($startTime<$quizSingleAttemptTimeStampFinishedchecking){
                                $WeekPercentage=($currentWeekActivityCompletedCount/$count_activities[$studentID])*100;
                                $currentWeekPercentage[$studentID]=round($WeekPercentage);
                            }else{
                                $WeekPercentage=($currentWeekActivityCompletedCount/$lastWeekIncompleteActivities)*100;
                                $currentWeekPercentage[$studentID]=round($WeekPercentage);
                            }
                        }                        
                    }//foreach $quizAttemptTimeStampFinishedchecking   
                    if(is_null($currentWeekPercentage[$studentID])){
                        $currentWeekPercentage[$studentID]=0;
                    }
                }//if for student active status 
                }//elseif student role
                else{
                    echo "<h1> No user with Teacher and Student role</h1>";
                }
            }//foreach userIdDetails 
        }//foreach usersRoleDetails

        
            // Send test email.
            $fromemail = local_mailtest_generate_email_user($fromemail->email,'');   
            
            /**
             * sending email to multiple user
             * arshads@3esofttech.com,engg.arshad83@gmail.com
             */  
            foreach($teacherEmails as $key=>$teacherEmail){
                $toemail = local_mailtest_generate_email_user($teacherEmail, '');
                $teacherFirstName=$teachersFirstname[$key];
                $teacherLastName=$teachersLastname[$key];
                $subject_message="$teacherFirstName $teacherLastName, Your Group's $course_short_name course progress report";
                $subject =format_string($subject_message);
                // Add some system information.
            
                
                $message='
                <div class="container" style="font-family:Arial;font-size:14px;">
                    <div style="text-align:center">
                    <img src="https://engineers.academy/wp-content/uploads/2017/03/Eng_ac_logo_final_smal.png" alt="engineersAcademy">
                    </div>
                    <p>Hi '.$teachersFirstname[$key].',</p>                  
                    
                    <p class="course-date">This weekly report is for the following Engineers Academy unit / course, for the week ending <span class="course-date">'. $date.' :</span></p>
                    <p>'.$course_name.'</p>
                    <p>Your students\' weekly and overall progress has been summarised in the table below:</p>';   
                
                if(empty($studentsFirstname)){
                     $message.="<h4> No Students in your Group: $group_name</h4>";  
                }else{
                $message.='<table  style="font-weight:400;font-family: Arial;border: 1px solid black; width: 100%;">
                    <thead>
                        <tr>
                            <th style="border: 1px solid #dddddd;
                            text-align: left;
                            padding: 8px;
                            font-weight:800;
                            background-color:black;
                            color:white;">Name</th>
                            <th style="border: 1px solid #dddddd;
                            text-align: left;
                            padding: 8px;
                            font-weight:800;
                            background-color:black;
                            color:white;">Weekly Progress</th> 
                            <th style="border: 1px solid #dddddd;
                            text-align: left;
                            padding: 8px;
                            font-weight:800;
                            background-color:black;
                            color:white;">Overall Progress</th>
                        </tr>
                    </thead>';
                foreach($studentsFirstname as $key=>$studentFirstname){                    
                $message.='<tr>
                        <td style="border: 1px solid #dddddd;
                        text-align: left;
                        padding: 8px;
                        ">'.$studentsFirstname[$key].' '.$studentsLastname[$key].'</td>
                        <td style="border: 1px solid #dddddd;
                        text-align: left;
                        padding: 8px;
                        ">'.$currentWeekPercentage[$key].'%</td>
                        <td style="border: 1px solid #dddddd;
                        text-align: left;
                        padding: 8px;
                        ">'.$resultCourseCompletedPercentage[$key].'%</td>
                        
                        </tr>';
                }
            }
                $message.='</table>
                </div>
                <br>
                <div class="footer" style="font-family:Arial;font-size:14px;">    
                <b><i>Disclaimer:</i></b>
                
              <span><i>You have received this email because you are appointed as a manager or mentor
                for a group of students enrolled on a unit / course with the Engineers Academy. If you no longer wish to receive weekly reports for your group, then please email us on <strong>weeklyreports@engineers.academy</strong> to request removal from the reporting schedule. </i> 
                </span>
                </div>';
    
                $messagetext=html_to_text($message);
                
                $success = email_to_user($toemail, $fromemail, $subject, $messagetext,  $message, '', '', true);  

                
            }//foreach template
            
        
    }//single group
    
}//else

}// send_courseId_to_emails
