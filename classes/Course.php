<?php

    function get_all_courses(){
		global $DB;
        $courses = get_courses();
        static $serial_number=1;
        $courseData=array();
        foreach ($courses as $course) {        
            if($course->id!=1){  
                
                $course = $DB->get_record('course', array('id'=> $course->id));
                if(!empty($course->enablecompletion)){
                    $courseData[] = array('id'=>$course->id,'name'=>$course->fullname);
                }           
                
            }
        }
        return $courseData;
    }
